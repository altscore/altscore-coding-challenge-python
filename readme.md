#Hello! 
please place your code on the `server/models/process_csv.py` file. The FastApi server is configured to run using the
`process_batch_from_csv` function. You can test your implementation with the test `test_process_csv.py` on server/tests.

